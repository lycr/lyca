import Vue from 'vue'
import Router from 'vue-router'
import all from '@/components/all'
import one from '@/components/one'
import tab from '../components/tab.vue'
import detail from '../components/detail.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      redirect: tab
    },
    {
      path: '/all',
      name: 'all',
      component: all
    },
    {
      path: '/tab',
      name: 'tab',
      component: tab
    },
    {
      path: '/all/one',
      name: 'one',
      component: one
    },
    {
      path: '/tab/detail',
      name: 'detail',
      component: detail
    }
  ]
})
